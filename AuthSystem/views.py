from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, models
from django.contrib.auth.models import User
from django.db import IntegrityError

# Create your views here.
def loginPage(request):
    if request.method == "POST":
        loginUsername = request.POST['loginName']
        loginPassword = request.POST['loginPass']
        user = authenticate(request, username=loginUsername, password=loginPassword)

        if user is not None:
            login(request, user)
            return redirect('home/')

    return render(request, 'loginPage.html')

def signupPage(request):
    if request.method == "POST":
        signupUsername = request.POST['signupName']
        signupEmail = request.POST['signupEmail']
        signupPassword = request.POST['signupPass']

        try:
            user = User.objects.create_user(username=signupUsername, password=signupPassword, email=signupEmail)
        except IntegrityError:
            return render(request, 'signupPage.html')
        
        user.save()
        return redirect('/')

    return render(request, 'signupPage.html')

def homePage(request):
    return render(request, 'homePage.html')

def logoutPage(request):
    if request.method == "POST":
        if request.POST['logout'] == 'Yes':
            logout(request)
            return redirect('/')
        elif request.POST['logout'] == 'No':
            return render(request, 'homePage.html')
    
    return render(request, 'logoutPage.html')
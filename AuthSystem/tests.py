from django.test import TestCase, Client
from django.urls import resolve
from .views import homePage, loginPage, logoutPage, signupPage, homePage
from django.contrib.auth.models import User

# Create your tests here.
class AuthSystemTest(TestCase):
    def test_url_is_not_exist(self):
        response = Client().get('/inigaada/')
        self.assertEqual(response.status_code, 404)

    def test_login_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_login_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'loginPage.html')

    def test_using_loginPage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, loginPage)

    def test_login_failed(self):
        response = self.client.post('/', data={'loginName':'kapewe', 'loginPass':'akusukapepewe'})
        self.assertEqual(response.status_code, 200)

    def test_logout_url_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)

    def test_using_logoutPage_func(self):
         found = resolve('/logout/')
         self.assertEqual(found.func, logoutPage)

    def test_signup_url_is_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_using_signupPage_func(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signupPage)

    def test_homepage_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_using_homePage_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, homePage)
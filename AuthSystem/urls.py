from django.urls import path
from . import views

app_name = 'AuthSystem'

urlpatterns = [
    path('', views.loginPage, name='loginPage'),
    path('signup/', views.signupPage, name='signupPage'),
    path('home/', views.homePage, name='homePage'),
    path('logout/', views.logoutPage, name='logoutPage'),
]